
import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class NotificationService {
  notifier = new EventEmitter<any>();


  notify(message: string): void {
    this.notifier.emit(message);
  }
}
